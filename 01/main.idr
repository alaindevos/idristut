module Main
import Data.String  -- parseInteger
import Data.Vect
{-
    Multiline commment
    Multiline commment
-}

a : String       ; a="1234567890123456789"
b : Maybe Int    ; b=parseInteger a
c : String       ; c=case b of 
                        Nothing => ""
                        Just i => show i

d : String       ; d="1e+300"
e : Maybe Double ; e=parseDouble d
f : String       ; f=case e of 
                        Nothing => ""
                        Just d => show d
                        
g : Nat          ; g=123456789012345678901234567890
h : String       ; h=show g

print1 : IO ()
print1 =  let mystring = "mystring" in 
        do
            putStrLn mystring

print2 : IO ()
print2 =  let mystring = "mystring" in 
        do
            putStrLn mystring
            putStrLn c
            putStrLn f
            putStrLn h

main : IO ()
main = do
            print1 
            print2 

maxBits8 : Bits8
maxBits8 = 255 

distanceToMax : Bits8 -> Bits8
distanceToMax n = maxBits8-n

square : Integer -> Integer
square n = n*n

times2 : Integer -> Integer
times2 n = 2*n 

pyth : Integer -> Integer -> Integer -> Bool 
pyth x y z = x*x+y*y == z*z 

squareTimes2a : Integer -> Integer
squareTimes2a = times2 . square

squareTimes2b : Integer -> Integer
squareTimes2b n = times2 (square n)

isEven : Integer -> Bool
isEven n = (mod n 2)==0

testSquare : (Integer -> Bool) -> Integer -> Bool
testSquare f n = f (square n)

resa : Bool 
resa = testSquare isEven 3

resb : Bool
resb = testSquare (\n => (mod n 2) == 0)  3

twice2 : Integer -> Integer
twice2 = \x => x+x

twice3 : Num ty => ty -> ty -- constraint ty is Num
twice3 = \x => x+x


resb2 : Integer
resb2 =twice2 5


twice : (Integer -> Integer) -> Integer -> Integer
twice f n  = f (f n)

resc : Integer
resc = twice square 3

partialExample : Integer -> Bool
partialExample = pyth 3 4

resd : Bool
resd = partialExample 5

identity1: ty -> ty 
identity1 x = x 

identity2 : (ty:Type) -> ty -> ty 
identity2 ty x = x 

infixl 4 >>> -- associate to the left x >>> y >>> z  === ((x >>> y) >>> z)
(>>>) : (Bits8 -> Bits8) -> (Bits8 -> Bits8) -> (Bits8 -> Bits8)
f >>> g = g . f

foo : Bits8 -> Bits8
foo n = 2*n+3

testin : Bits8 -> Bits8
test = foo >>> foo >>> foo 

infixl 8 `plus`

plus : Integer -> Integer -> Integer
plus = (+)

rese : Integer
rese = 3 `plus`5

data Weekendday = Saterday | Sunday

total
nextday : Weekendday -> Weekendday
nextday Saterday = Sunday
nextday Sunday = Saterday

total
isSaterday : Weekendday -> Bool
isSaterday Saterday = True
isSaterday _ = False

total
isGreatera : Bits8 -> Bits8 -> String
isGreatera x y =
    case (y > x) of
        True => "True"
        False => "False"

total
isGreaterb : Bits8 -> Bits8 -> String
isGreaterb x y =
    if (y > x) 
    then
        "True"
    else
        "False"

data Title = Mr | Mrs | Other String

total
dr : Title
dr = Other "Dr."

total
showTitle : Title -> String
showTitle Mr = "Mr."
showTitle Mrs = "Mrs."
showTitle (Other s) = s

total
greet : Title -> String -> String
greet t n = "Hello " ++ showTitle t ++ n

record User where
    constructor MkUser
    name : String
    title : Title
    age : Bits8

total
alain : User
alain = MkUser "Alain" (Other "Agent") 51

total
getNamea : User -> String
getNamea (MkUser n _ _) = n

getNameb : User -> String
getNameb u = name u

getNamec : User -> String
getNamec u = u.name

total
incAgea : User -> User
incAgea (MkUser name title age) = MkUser name title (age+1)

total
incAgeb : User -> User
incAgeb u = {age := u.age+1} u

record Person where
    constructor MkPerson
    name : String
    age : Integer

total
makeP : String -> Integer -> (String,Integer) 
makeP n a = (n,a)

total
makeP2 : String -> Integer -> Person
makeP2 n a = MkPerson n a

total
p : (String,Integer)
p = ("Alain",52)

total
getN : String
getN = case p of 
    (s,a) => s

data Option aa = Some aa | None

total
readBool : String -> Option Bool 
readBool "True"  = Some True
readBool "False" = Some False
readBool _ = None

safeDiv : Integer -> Integer -> Option Integer
safeDiv n 0 = None
safeDiv n k = Some (n `div` k)

data Validated ee aa = Invalid ee | Valid aa

total
isSaterday2 : String -> Validated String Weekendday
isSaterday2 "Saterday" =  Valid Saterday
isSaterday2 "Sunday"    =  Invalid "Is Sunday"
isSaterday2 _           =  Invalid "Is Weekday"

data MySeq aa = Nil | (::) aa (MySeq aa)
total
myToList : Vect n Nat -> MySeq Nat
myToList [] = Nil
myToList (x::xs) = (x :: (myToList xs))

total
myints : MySeq Integer
myints = 1 :: 2 :: 3 :: Nil 

total
ints : List Int64
ints = 1 :: 2 :: 3 :: Nil

total
ints2 : List Int64
ints2 = [1,2,3]

total
ints3 : List Int64
ints3 = []

total
intSum : List Integer -> Integer
intSum Nil = 0
intSum (x :: lis)= x+ intSum lis

resf : Integer 
resf =  let 
            x : Integer
            x = 5
        in 
            x + x 

resg : Integer 
resg = x + x 
        where 
            x : Integer 
            x = 5 

resh : ( Integer , String )
resh = ( 52 , "Alain" )

resi : Integer 
resi = fst resh

resj : String
resj = snd resh 

resk : List String 
resk = [ "Alain" , "De  Vos"]

resl : List Nat 
resl = map length resk 

resm : List Integer 
resm = [ 1 ,2 , 3 , 4 , 5]

resn : List Integer 
resn = filter (\x => x > 3 ) resm  

describeList2 : List Integer -> String 
describeList2 [] = "Empty"
describeList2 (x::xs) = "Non-Empty"

allLengths3 : List String -> List Nat
allLengths3 Nil = Nil
allLengths3 (x::xs)=(length x) :: (allLengths3 xs)

xor : Bool -> Bool -> Bool 
xor False x = x
xor True x = not x 

fourInts : Vect 4 Int 
fourInts = [ 1 , 2 , 3 , 4]

sixInts : Vect 6 Int 
sixInts = [ 5 , 6 , 7 , 8 , 9 , 10]

tenInts : Vect 10 Int 
tenInts = fourInts ++ sixInts 

allLengths : Vect n String -> Vect n Nat 
allLengths Nil = Nil
allLengths (x::xs) = (length x) ::  (allLengths xs)

testje : Integer 

myCopyz : (t : Type) => Vect n t  -> Vect n t
myCopyz Nil = Nil 
myCopyz (x::xs)  = x :: xs 

myAddq : (t : Type) => t -> Vect n t  -> Vect (n+1) t
myAddq  i Nil = [i]  
myAddq  i (x::xs)  = x :: (myAddq i xs)

tri: Vect 3 ( Double , Double )
tri = [(1.0,1.0),(2.0,2.0),(3.0,3.0)]

MyPosition:Type
MyPosition=(Double,Double)

tri3: Vect 3 MyPosition
tri3 = [(1.0,1.0),(2.0,2.0),(3.0,3.0)]

Polygon : Nat -> Type
Polygon n = Vect n MyPosition

tribis : Polygon 3
tribis = [(1.0,1.0),(2.0,2.0),(3.0,3.0)]

StringOrInt : Bool -> Type 
StringOrInt False = String 
StringOrInt True = Int 

getStringOrInt : (isInt : Bool) -> (StringOrInt isInt)
getStringOrInt False = "Ninety Four"
getStringOrInt True = 94

valToString : (isInt : Bool) -> (StringOrInt isInt) -> String
valToString False y = trim y  --remove spaces
valToString True y = cast y   --convert to string

data Format =     Num Format 
                | Str Format
                | Lit String Format
                | End

PrintfType : Format -> Type 
PrintfType (Num fmt)= (i:Int) -> PrintfType fmt
PrintfType (Str fmt)= (s:String) -> PrintfType fmt
PrintfType (Lit str fmt) = PrintfType fmt
PrintfType End = String 

printfFmt : (fmt:Format)->(acc:String)->PrintfType fmt 
printfFmt (Num fmt) acc =
    \i => printfFmt fmt (acc ++ show i)
printfFmt (Str fmt) acc =
    \s => printfFmt fmt (acc ++ s)
printfFmt (Lit lit fmt) acc = 
    printfFmt fmt (acc++lit)
printfFmt End acc = acc 

toFormat : (xs:List Char) -> Format 
toFormat [] = End 
toFormat ('%'::'d'::chars)=Num (toFormat chars)
toFormat ('%'::'s'::chars)=Str (toFormat chars)
toFormat ('%'::chars)=Lit "%" (toFormat chars)
toFormat (c::chars)= case toFormat chars of
    Lit lit chars => Lit (strCons c lit) chars
    fmt => Lit (strCons c "") fmt 

printf : (fmt:String) -> PrintfType (toFormat (unpack fmt))
printf fmt = printfFmt _ ""

mirror : List aa -> List aa
mirror xs  = let xs2 = reverse xs in
                xs++xs2

pythag : Int -> List (Int,Int,Int)
pythag n = [ (x,y,z) | z<- [1..n] , y<-[1..z] , x<- [1..y] , x*x+y*y==z*z]

interface MyFunctor (f:Type->Type) where
    mymap : (m:a->b)-> f a -> f b 
MyFunctor List where
    mymap f [] = []
    mymap f (x::xs) = (f x) :: (mymap f xs)
    
data MyNat = Z | S MyNat 
myplus : MyNat -> MyNat -> MyNat
myplus Z x = x 
myplus (S x) y = S (myplus x y)

myreverse: List a -> List a 
myreverse xs= revAcc [] xs where 
    revAcc : List a -> List a -> List a 
    revAcc acc [] = acc 
    revAcc acc (x::xs) = revAcc (x::acc) xs 

myEven: MyNat -> Bool 
myEven Z = True 
myEven (S x) = myOdd x where 
    myOdd : MyNat -> Bool
    myOdd Z = False 
    myOdd (S y) = myEven y  

data MyVect : Nat -> Type -> Type where 
    MyNil : MyVect Z aa
    MyS : aa -> MyVect k aa -> MyVect (S k) aa 

myAppend2 : MyVect n aa -> MyVect m aa -> MyVect (n+m) aa
myAppend2 MyNil ys = ys
myAppend2 (MyS x xs) ys = MyS x (myAppend2 xs ys)

interface MyShow a where 
    myshow : a -> String 

MyShow Nat where 
    myshow Z = "Z"
    myshow (S x)="S" ++ myshow x 

total
myshow1 : List Int -> String 
myshow1 Nil = "" 
myshow1 (x::xs) = (show x) ++ ", " ++ (myshow1 xs)
total
myshow2 : List Int -> String
myshow2 s = "["++(myshow1 s) ++"]"

ssss: String
ssss = myshow2 [1,2,3,4]



myFromList : (xs:List aa) -> Vect (length xs) aa
myFromList Nil = []
myFromList (x::xs) = snoc (myFromList xs) x


